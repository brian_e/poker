#! /usr/bin/env python3

from collections import namedtuple
from itertools import groupby
from functools import total_ordering

_POSSIBLE_STRAIGHTS = [tuple(range(i + 5, i, -1)) for i in range(9, 0, -1)] + [(5, 4, 3, 2, 14)]

def _find_best_straight(ranks):
    for possible_straight in _POSSIBLE_STRAIGHTS:
        if all(rank in ranks for rank in possible_straight):
            return possible_straight
    return ()

def _to_ranks(cards, included_suits='schd'):
    def to_rank(rank_char):
        try:
            return int(rank_char)
        except ValueError:
            return 10 + 'TJQKA'.index(rank_char)
    def generate_ranks():
        for rank_char, suit in zip(cards[::2], cards[1::2]):
            if suit in included_suits:
                yield to_rank(rank_char)
    return tuple(sorted(generate_ranks(), reverse=True))


HandValue = namedtuple('HandValue', ['straight_flush', 'quads', 'boat',
                                     'flush', 'straight', 'trips',
                                     'pair_count', 'played_ranks'])


@total_ordering
class Hand:

    def __init__(self, cards):
        all_ranks = _to_ranks(cards)
        flush_suits = [s for s, g in groupby(sorted(s for s in cards[1::2])) if len(list(g)) > 4]
        rank_groups = tuple(sorted((tuple(g) for k, g in groupby(all_ranks)),
                                   key=lambda g: (len(g), g),
                                   reverse=True))
        played_ranks = tuple(rank for rank_group in rank_groups for rank in rank_group)[:5]
        dup_counts = {k: len([r for r in rank_groups if len(r) == k]) for k in (2, 3, 4)}
        flush = _to_ranks(cards, flush_suits) if flush_suits else ()
        self.value = HandValue(
            straight_flush=_find_best_straight(flush) if flush_suits else (),
            quads=played_ranks if dup_counts[4] else (),
            boat=played_ranks if (dup_counts[3] and dup_counts[3] + dup_counts[2] > 1) else (),
            flush=flush,
            straight=_find_best_straight(all_ranks),
            trips=played_ranks if dup_counts[3] else (),
            pair_count=dup_counts[2],
            played_ranks=played_ranks)

    def __repr__(self):
        return str(self.value)

    def __str__(self):
        for k, val in self.value._asdict().items():
            if val:
                return '{} {}'.format(k, val)

    def __eq__(self, other):
        return self.value == other.value

    def __lt__(self, other):
        return self.value < other.value


def main():
    hand = None
    for cards in 'AsJsAcAdKsTsQs', '2c3d4h5d7c8d9h', 'AcAdAhKsKdTd2d', '7c7d7hKc9cTc2c':
        if hand:
            print()
            print('  is greater than...' if (hand > Hand(cards)) else '  is less than...')
        hand = Hand(cards)
        print(cards)
        print(hand)
        print(repr(hand))

if __name__ == '__main__':
    main()


def test_high_card():
    ace_high = Hand('AsKsQsTs9d')
    ace_high_b = Hand('AdKdQdTd9s')
    assert ace_high == ace_high_b
    king_high = Hand('KdQdJd9s8d')
    assert ace_high > king_high
    ace_high_q = Hand('AdQdJd9s8s')
    assert ace_high > ace_high_q
    assert Hand('2c3c4c6c7h') > Hand('2h3h4h5h7c')

def test_one_pair():
    pair_2s = Hand('8s2sQs2d9d')
    ace_high = Hand('AcKcQcJc9h')
    assert pair_2s > ace_high
    pair_3s = Hand('8d3d3d2h9h')
    assert pair_3s > pair_2s
    assert pair_2s > Hand('8d2dJd2s9s')

def test_two_pair():
    fours_over_2s = Hand('4s2s4d5h2h')
    assert fours_over_2s > Hand('AsKsQsJs9d')
    assert fours_over_2s > Hand('8dAdAd2c9h')
    assert fours_over_2s > Hand('3s2s4d3h2h')
    assert fours_over_2s > Hand('4s2s4d3h2h')

def test_trips():
    trip_3s = Hand('3s3c3d5h4h')
    assert trip_3s > Hand('AsKsQsJs9d')
    assert trip_3s > Hand('8dAdAd2c9h')
    assert trip_3s > Hand('4s2s4d5h2h')
    assert trip_3s > Hand('3s3c3d5h2h')

def test_straight():
    wheel = Hand('As2d3d4d5d')
    print(wheel, Hand('AsKsQsJs9d'))
    assert wheel > Hand('AsKsQsJs9d')
    assert wheel > Hand('8dAdAd2c9h')
    assert wheel > Hand('4s2s4d5h2h')
    assert wheel > Hand('3s3c3d5h4h')
    assert Hand('2d3d6d4c5d') > wheel

def test_flush():
    flush = Hand('TdJd2d4d7d')
    assert flush > Hand('AsKsQsJs9d')
    assert flush > Hand('8dAdAd2c9h')
    assert flush > Hand('4s2s4d5h2h')
    assert flush > Hand('3s3c3d5h4h')
    assert flush > Hand('2d3d6c4d5d')
    assert Hand('AsKs2sTs5s') > flush

def test_boat():
    boat = Hand('6h6c9c6d9h')
    assert boat > Hand('AsKsQsJs9d')
    assert boat > Hand('8dAdAd2c9h')
    assert boat > Hand('4s2s4d5h2h')
    assert boat > Hand('3s3c3d5h4h')
    assert boat > Hand('2d3d6c4d5d')
    assert boat > Hand('AsKs2sTs5s')
    assert Hand('2d2c8c8d8h') > boat

def test_quads():
    quads = Hand('6h6c9c6d6s')
    assert quads > Hand('AsKsQsJs9d')
    assert quads > Hand('8dAdAd2c9h')
    assert quads > Hand('4s2s4d5h2h')
    assert quads > Hand('3s3c3d5h4h')
    assert quads > Hand('2d3d6c4d5d')
    assert quads > Hand('AsKs2sTs5s')
    assert quads > Hand('2d2c8c8d8h')
    assert Hand('6h6cTc6d6s') > quads
    assert Hand('7h7c9c7d7s') > quads

def test_straight_flush():
    straight_flush = Hand('6d9d8d5d7d')
    assert straight_flush > Hand('AsKsQsJs9d')
    assert straight_flush > Hand('8dAdAd2c9h')
    assert straight_flush > Hand('4s2s4d5h2h')
    assert straight_flush > Hand('3s3c3d5h4h')
    assert straight_flush > Hand('2d3d6c4d5d')
    assert straight_flush > Hand('AsKs2sTs5s')
    assert straight_flush > Hand('2d2c8c8d8h')
    assert straight_flush > Hand('6h6cTc6d6s')
    assert Hand('AdKdQdTdJd') > straight_flush

def test_7_high_card():
    ace_high = Hand('AsKsQsTs9d2c3d')
    ace_high_b = Hand('AdKdQdTd9s2c3c')
    assert ace_high == ace_high_b
    king_high = Hand('KdQdJd9s8d2c3c')
    assert ace_high > king_high
    ace_high_q = Hand('AdQdJd9s8s2c3d')
    assert ace_high > ace_high_q
    assert Hand('2c3c4c6c7h2c3d') > Hand('2h3h4h5h7c2c3d')

def test_7_one_pair():
    pair_2s = Hand('8s2sQs3d9d2c7s')
    ace_high = Hand('2c7sAcKdQcJc9h')
    assert pair_2s > ace_high
    pair_3s = Hand('8d3d2c7s3d2h9h')
    assert pair_3s > pair_2s
    assert pair_2s > Hand('8d2dJd2c7s3s9s')

def test_7_two_pair():
    fours_over_2s = Hand('4s2s4d5h2h6cJc')
    assert fours_over_2s > Hand('AsKsQsJs9d6cJc')
    assert fours_over_2s > Hand('8dAdAd2c9h6cJc')
    assert fours_over_2s > Hand('3s2s4d3h2h6cJc')
    assert fours_over_2s == Hand('4s2s4d3h2h6cJc')
    assert fours_over_2s > Hand('4s2s4d3h2h6cTc')

def test_7_trips():
    trip_3s = Hand('3s3c3d5h4h6cJc')
    assert trip_3s > Hand('AsKsQsJs9d6cJc')
    assert trip_3s > Hand('8dAdAd2c9h6cJc')
    assert trip_3s > Hand('4s2s4d5h2h6cJc')
    assert trip_3s == Hand('3s3c3d5h2h6cJc')
    assert trip_3s > Hand('3s3c3d5h2h6cTc')

def test_7_straight():
    wheel = Hand('As2d3d4d5d3cJc')
    assert wheel > Hand('AsKsQsJs9d6cJc')
    assert wheel > Hand('8dAdAd2c9h6cJc')
    assert wheel > Hand('4s2s4d5h2h6cJc')
    assert wheel > Hand('3s3c3d5h4h6cJc')
    assert Hand('2d3d6d4c5d6cJc') > wheel
    assert Hand('2d3c4d5c6d7c8d') > Hand('Td3c4d5c6d7cTd')

def test_7_flush():
    flush = Hand('TdJd2d4d7dAcJc')
    assert flush > Hand('AsKsQsJs9d6cJc')
    assert flush > Hand('8dAdAd2c9h6cJc')
    assert flush > Hand('4s2s4d5h2h6cJc')
    assert flush > Hand('3s3c3d5h4h6cJc')
    assert flush > Hand('2d3d6c4d5d6cJc')
    assert Hand('QsKs2sTs5s6cJc') > flush

def test_7_boat():
    boat = Hand('6h7c9c6d9h6cJc')
    assert boat > Hand('AsKsQsJs9d6cJc')
    assert boat > Hand('8dAdAd2c9h6cJc')
    assert boat > Hand('4s2s4d5h2h6cJc')
    assert boat > Hand('3s3c3d5h4h6cJc')
    assert boat > Hand('2d3d6c4d5d6cJc')
    assert boat > Hand('AsKs2sTs5s6cJc')
    assert Hand('2d2c8c8d8h6cJc') > boat
    assert Hand('AcAdAhKsKdTd2d') == Hand('AcAdAhKsKd6c4c')

def test_7_quads():
    quads = Hand('6h6c9c6d6s7cJc')
    assert quads > Hand('AsKsQsJs9d6cJc')
    assert quads > Hand('8dAdAd2c9h6cJc')
    assert quads > Hand('4s2s4d5h2h6cJc')
    assert quads > Hand('3s3c3d5h4h6cJc')
    assert quads > Hand('2d3d6c4d5d6cJc')
    assert quads > Hand('AsKs2sTs5s6cJc')
    assert quads > Hand('2d2c8c8d8h6cJc')
    assert Hand('6h6cTc6d6s7cJc') == quads
    assert Hand('6h6cTc6d6s7cQc') > quads
    assert Hand('7h7c9c7d7s6cJc') > quads

def test_7_straight_flush():
    straight_flush = Hand('6d9d8d5d7d6cJc')
    assert straight_flush > Hand('AsKsQsJs9d6cJc')
    assert straight_flush > Hand('8dAdAd2c9h6cJc')
    assert straight_flush > Hand('4s2s4d5h2h6cJc')
    assert straight_flush > Hand('3s3c3d5h4h6cJc')
    assert straight_flush > Hand('2d3d6c4d5d6cJc')
    assert straight_flush > Hand('AsKs2sTs5s6cJc')
    assert straight_flush > Hand('2d2c8c8d8h6cJc')
    assert straight_flush > Hand('6h6cTc6d6s6cJc')
    assert Hand('AdKdQdTdJd6cJc') > straight_flush
